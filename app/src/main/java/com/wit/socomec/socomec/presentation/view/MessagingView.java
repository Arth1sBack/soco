package com.wit.socomec.socomec.presentation.view;

import com.wit.socomec.socomec.data.model.Choice;
import com.wit.socomec.socomec.presentation.viewmodel.MessageViewModel;

public interface MessagingView {
    void updateViewWithChoice(Choice choice);
    void postMessage(MessageViewModel messageViewModel);
}
