package com.wit.socomec.socomec.presentation;

import android.app.Application;

import com.wit.socomec.socomec.data.manager.ApiManager;
import com.wit.socomec.socomec.data.manager.ApiManagerImpl;
import com.wit.socomec.socomec.data.manager.CacheManager;
import com.wit.socomec.socomec.data.manager.CacheManagerImpl;
import com.wit.socomec.socomec.data.repository.Repository;

public class SocApplication extends Application {

    private static SocApplication application;
    public static SocApplication application() {
        return application;
    }

    private Repository repository;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;
        initializeInjection();
    }

    public Repository getRepository() {
        return repository;
    }

    private void initializeInjection() {
        final ApiManager apiManager = new ApiManagerImpl();
        final CacheManager cacheManager = new CacheManagerImpl();
        this.repository = new Repository(apiManager, cacheManager);
    }
}
