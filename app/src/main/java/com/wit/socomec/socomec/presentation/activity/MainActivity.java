package com.wit.socomec.socomec.presentation.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.presentation.SocApplication;
import com.wit.socomec.socomec.presentation.listener.MainNavigatorListener;
import com.wit.socomec.socomec.presentation.navigator.MainNavigator;
import com.wit.socomec.socomec.presentation.presenter.MainPresenter;
import com.wit.socomec.socomec.presentation.view.MainView;

public class MainActivity extends AppCompatActivity implements MainView, MainNavigatorListener {

    private MainNavigator mainNavigator;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeInjection();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainNavigator.displayInitialFragment();
    }

    @Override
    public void displayMessagingComponent() {
        mainNavigator.displayMessagingFragment();
    }

    private void initializeInjection() {
        this.mainNavigator = new MainNavigator(this);
        this.mainPresenter = new MainPresenter(SocApplication.application().getRepository(), this, mainNavigator);
    }
}
