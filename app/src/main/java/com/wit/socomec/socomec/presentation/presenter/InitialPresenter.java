package com.wit.socomec.socomec.presentation.presenter;

import com.wit.socomec.socomec.data.model.Choice;
import com.wit.socomec.socomec.data.repository.Repository;
import com.wit.socomec.socomec.presentation.listener.MainNavigatorListener;
import com.wit.socomec.socomec.presentation.navigator.MainNavigator;
import com.wit.socomec.socomec.presentation.view.InitialView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class InitialPresenter {
    final Repository repository;
    final InitialView initialView;
    final MainNavigatorListener mainNavigatorListener;

    public InitialPresenter(Repository repository, InitialView initialView, MainNavigatorListener mainNavigatorListener) {
        this.repository = repository;
        this.initialView = initialView;
        this.mainNavigatorListener = mainNavigatorListener;
    }

    public void onStart() {
        retrieveChoiceList();
    }

    public void displayMessagingComponent() {
        mainNavigatorListener.displayMessagingComponent();
    }

    private void retrieveChoiceList() {
        repository.retrieveChoiceList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<Choice>>() {
                    @Override
                    public void onNext(List<Choice> choiceList) {
                    }
                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onComplete() {
                        initialView.activateStartButton();
                    }
                });
    }
}
