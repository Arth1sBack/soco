package com.wit.socomec.socomec.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.data.model.Choice;
import com.wit.socomec.socomec.presentation.SocApplication;
import com.wit.socomec.socomec.presentation.adapter.DiscussionListAdapter;
import com.wit.socomec.socomec.presentation.listener.MainNavigatorListener;
import com.wit.socomec.socomec.presentation.presenter.MessagingPresenter;
import com.wit.socomec.socomec.presentation.view.MessagingView;
import com.wit.socomec.socomec.presentation.viewmodel.MessageViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessagingFragment extends Fragment implements MessagingView {

    @BindView(R.id.fragment_messaging_button_answer1)
    Button buttonAnswer1;
    @BindView(R.id.fragment_messaging_button_answer2)
    Button buttonAnswer2;
    @BindView(R.id.fragment_messaging_button_previous)
    Button buttonPrevious;
    @BindView(R.id.fragment_messaging_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fragment_messaging_linearlayout)
    LinearLayout linearLayout;

    private MessagingPresenter messagingPresenter;
    private DiscussionListAdapter discussionListAdapter;

    public static MessagingFragment newInstance() {
        Bundle args = new Bundle();
        MessagingFragment fragment = new MessagingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messaging, container, false);
        ButterKnife.bind(this, view);
        initializeInjection();
        initListView();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        messagingPresenter.onStart();
    }

    @Override
    public void updateViewWithChoice(Choice choice) {
        if(choice.getNextStep1() == 0 && choice.getNextStep2() == 0){
            linearLayout.setVisibility(View.GONE);
            return;
        }

        buttonAnswer1.setText(choice.getAnswer1());
        buttonAnswer2.setText(choice.getAnswer2());

        if(choice.getPreviousStep() == null) {
            buttonPrevious.setVisibility(View.GONE);
        } else {
            buttonPrevious.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void postMessage(MessageViewModel messageViewModel) {
        discussionListAdapter.addMessage(messageViewModel);

        recyclerView.scrollToPosition(discussionListAdapter.getItemCount()-1);
    }

    @OnClick(R.id.fragment_messaging_button_answer1)
    public void clickOnAnswer1() {
        messagingPresenter.loadNextStep1();
    }

    @OnClick(R.id.fragment_messaging_button_answer2)
    public void clickOnAnswer2() {
        messagingPresenter.loadNextStep2();
    }

    @OnClick(R.id.fragment_messaging_button_previous)
    public void clickOnPrevious() {
        messagingPresenter.loadPreviousStep();
    }

    private void initListView(){
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(discussionListAdapter);
    }

    private void initializeInjection() {
        if(getActivity() instanceof MainNavigatorListener) {
            this.messagingPresenter = new MessagingPresenter(SocApplication.application().getRepository(), this, (MainNavigatorListener)getActivity());
            this.discussionListAdapter = new DiscussionListAdapter();
        } else {
            Log.e("injection error", "MessagingFragment");
        }
    }
}
