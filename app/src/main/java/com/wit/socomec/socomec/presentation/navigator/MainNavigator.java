package com.wit.socomec.socomec.presentation.navigator;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.presentation.fragment.InitialFragment;
import com.wit.socomec.socomec.presentation.fragment.MessagingFragment;

public class MainNavigator {
    private final Activity activity;
    private final FragmentManager fragmentManager;

    public MainNavigator(FragmentActivity activity) {
        this.activity = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    public void displayInitialFragment() {
        fragmentTransaction(InitialFragment.newInstance());
    }

    public void displayMessagingFragment() {
        fragmentTransaction(MessagingFragment.newInstance());
    }

    private void fragmentTransaction(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        /*if(currentFragmentId != FRAGMENT_CLASS_LIST){
            fragmentTransaction.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right, R.animator.slide_in_right, R.animator.slide_out_left);
        }*/

        fragmentTransaction.add(R.id.activity_main_fame_layout, fragment, fragment.getTag());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }
}
