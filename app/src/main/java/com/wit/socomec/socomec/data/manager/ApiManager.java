package com.wit.socomec.socomec.data.manager;

import com.wit.socomec.socomec.data.model.Choice;

import java.util.List;

import io.reactivex.Observable;

public interface ApiManager {
    Observable<List<Choice>> retrieveChoiceList();
}
