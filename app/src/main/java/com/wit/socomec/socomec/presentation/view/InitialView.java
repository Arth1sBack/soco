package com.wit.socomec.socomec.presentation.view;

public interface InitialView {
    void activateStartButton();
}
