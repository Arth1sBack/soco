package com.wit.socomec.socomec.presentation.presenter;

import com.wit.socomec.socomec.data.repository.Repository;
import com.wit.socomec.socomec.presentation.navigator.MainNavigator;
import com.wit.socomec.socomec.presentation.view.MainView;

public class MainPresenter {
    final Repository repository;
    final MainView mainView;
    final MainNavigator mainNavigator;

    public MainPresenter(Repository repository, MainView mainView, MainNavigator mainNavigator) {
        this.repository = repository;
        this.mainView = mainView;
        this.mainNavigator = mainNavigator;
    }
}
