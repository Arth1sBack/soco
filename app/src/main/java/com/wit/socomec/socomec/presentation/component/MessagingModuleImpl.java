package com.wit.socomec.socomec.presentation.component;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by amiltonedev_lt030 on 05/04/2017.
 */

public class MessagingModuleImpl implements MessagingModule {
    private final Activity activity;

    public MessagingModuleImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void displayErrorMessage(View view, String message) {
        if(message != null && view != null){
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.getView().setBackgroundColor(activity.getResources().getColor(android.R.color.holo_red_light));
            snackbar.show();
        }
    }

    @Override
    public void displaySuccessMessage(View view, String message){
        if(message != null){
            Snackbar snackbar = Snackbar.make(view, message,
                    Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.getView().setBackgroundColor(activity.getResources().getColor(android.R.color.holo_green_dark));
            snackbar.show();
        }
    }
}
