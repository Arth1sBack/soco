package com.wit.socomec.socomec.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.presentation.SocApplication;
import com.wit.socomec.socomec.presentation.listener.MainNavigatorListener;
import com.wit.socomec.socomec.presentation.presenter.InitialPresenter;
import com.wit.socomec.socomec.presentation.view.InitialView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InitialFragment extends Fragment implements InitialView {

    @BindView(R.id.fragment_initial_button_start)
    Button buttonStart;

    private InitialPresenter initialPresenter;

    public static InitialFragment newInstance() {

        Bundle args = new Bundle();

        InitialFragment fragment = new InitialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_initial, container, false);
        ButterKnife.bind(this, view);
        initializeInjection();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialPresenter.onStart();
    }

    @Override
    public void activateStartButton() {
        buttonStart.setEnabled(true);
    }

    @OnClick(R.id.fragment_initial_button_start)
    public void clickOnStart() {
        initialPresenter.displayMessagingComponent();
    }

    private void initializeInjection() {
        if(getActivity() instanceof MainNavigatorListener) {
            this.initialPresenter = new InitialPresenter(SocApplication.application().getRepository(), this, (MainNavigatorListener)getActivity());
        } else {
            Log.e("injection error", "InitialFragment");
        }
    }
}
