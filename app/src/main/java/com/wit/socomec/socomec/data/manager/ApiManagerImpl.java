package com.wit.socomec.socomec.data.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wit.socomec.socomec.data.model.Choice;

import java.util.List;


import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class ApiManagerImpl implements ApiManager {

    private ApiService service;

    public interface ApiService {
        @GET("5bf047e72f000075117a0c66")
        Observable<List<Choice>> retrieveChoiceList();
    }

    public ApiManagerImpl() {
        Gson gson = new GsonBuilder()
                .create();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/v2/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
        service = retrofit.create(ApiService.class);
    }

    @Override
    public Observable<List<Choice>> retrieveChoiceList() {
        return service.retrieveChoiceList();
    }
}
