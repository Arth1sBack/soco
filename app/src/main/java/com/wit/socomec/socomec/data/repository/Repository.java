package com.wit.socomec.socomec.data.repository;

import com.wit.socomec.socomec.data.manager.ApiManager;
import com.wit.socomec.socomec.data.manager.CacheManager;
import com.wit.socomec.socomec.data.model.Choice;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;


public class Repository {
    final ApiManager apiManager;
    final CacheManager cacheManager;

    public Repository(ApiManager apiManager, CacheManager cacheManager) {
        this.apiManager = apiManager;
        this.cacheManager = cacheManager;
    }

    public Observable<List<Choice>> retrieveChoiceList() {
        return apiManager.retrieveChoiceList()
                .doOnNext(new Consumer<List<Choice>>() {
            @Override
            public void accept(List<Choice> choiceList) {
                cacheManager.saveChoiceList(choiceList);
            }
        });
    }

    public Observable<Choice> retriveChoiceForId(final int id) {
        return Observable.defer(new Callable<ObservableSource<Choice>>() {
            @Override
            public ObservableSource<Choice> call() {
                return Observable.just(cacheManager.getChoiceForId(id));
            }
        });
    }
}
