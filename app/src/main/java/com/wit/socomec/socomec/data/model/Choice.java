package com.wit.socomec.socomec.data.model;

import com.google.gson.annotations.SerializedName;

public class Choice {
    @SerializedName("id")
    private int id;
    @SerializedName("previous_step")
    private Integer previousStep;
    @SerializedName("question")
    private String question;
    @SerializedName("answer1")
    private String answer1;
    @SerializedName("next_step1")
    private int next_step1;
    @SerializedName("answer2")
    private String answer2;
    @SerializedName("next_step2")
    private int next_step2;

    public int getId() {
        return id;
    }

    public Integer getPreviousStep() {
        return previousStep;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public int getNextStep1() {
        return next_step1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public int getNextStep2() {
        return next_step2;
    }
}
