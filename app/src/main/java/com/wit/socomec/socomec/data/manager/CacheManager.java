package com.wit.socomec.socomec.data.manager;

import com.wit.socomec.socomec.data.model.Choice;

import java.util.List;

public interface CacheManager {
    void saveChoiceList(List<Choice> choiceList);
    Choice getChoiceForId(int id);
}
