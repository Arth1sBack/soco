package com.wit.socomec.socomec.data.manager;

import android.util.SparseArray;

import com.wit.socomec.socomec.data.model.Choice;


import java.util.List;

public class CacheManagerImpl implements CacheManager {
    private SparseArray<Choice> choiceMap;

    public CacheManagerImpl() {
        choiceMap = new SparseArray<>();
    }

    @Override
    public void saveChoiceList(List<Choice> choiceList) {
        choiceMap.clear();

        for(Choice choice : choiceList) {
            choiceMap.put(choice.getId(), choice);
        }
    }

    @Override
    public Choice getChoiceForId(int id) {
        return choiceMap.get(id);
    }
}
