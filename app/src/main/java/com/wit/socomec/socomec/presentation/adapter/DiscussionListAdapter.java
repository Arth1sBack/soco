package com.wit.socomec.socomec.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.presentation.viewholder.MessageViewHolder;
import com.wit.socomec.socomec.presentation.viewmodel.MessageViewModel;

import java.util.ArrayList;
import java.util.List;

public class DiscussionListAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    private List<MessageViewModel> messageViewModelList;

    public DiscussionListAdapter() {
        messageViewModelList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_message, viewGroup, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder viewHolder, int i) {
        viewHolder.setContent(messageViewModelList.get(i));
    }

    @Override
    public int getItemCount() {
        return messageViewModelList.size();
    }

    public void addMessage(MessageViewModel messageViewModel) {
        messageViewModelList.add(messageViewModel);
        notifyItemInserted(messageViewModelList.size() - 1);
    }
}
