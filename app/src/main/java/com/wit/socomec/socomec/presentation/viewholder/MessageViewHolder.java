package com.wit.socomec.socomec.presentation.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wit.socomec.socomec.R;
import com.wit.socomec.socomec.presentation.viewmodel.MessageViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.listitem_message_backgroud_answer)
    ImageView imageBackgroundAnswer;
    @BindView(R.id.listitem_message_backgroud_question)
    ImageView imageBackgroundQuestion;
    @BindView(R.id.listitem_message_text_answer)
    TextView textAnswer;
    @BindView(R.id.listitem_message_text_question)
    TextView textQuestion;

    public MessageViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setContent(MessageViewModel messageViewModel) {
        if(messageViewModel.isQuestion) {
            imageBackgroundQuestion.setVisibility(View.VISIBLE);
            textQuestion.setVisibility(View.VISIBLE);
            imageBackgroundAnswer.setVisibility(View.GONE);
            textAnswer.setVisibility(View.GONE);

            textQuestion.setText(messageViewModel.text);
        } else {
            imageBackgroundQuestion.setVisibility(View.GONE);
            textQuestion.setVisibility(View.GONE);
            imageBackgroundAnswer.setVisibility(View.VISIBLE);
            textAnswer.setVisibility(View.VISIBLE);

            textAnswer.setText(messageViewModel.text);
        }
    }
}
