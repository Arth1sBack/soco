package com.wit.socomec.socomec.presentation.viewmodel;

public class MessageViewModel {
    final public boolean isQuestion;
    final public String text;

    public MessageViewModel(boolean isQuestion, String text) {
        this.isQuestion = isQuestion;
        this.text = text;
    }
}
