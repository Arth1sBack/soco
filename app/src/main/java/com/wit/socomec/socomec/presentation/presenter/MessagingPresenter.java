package com.wit.socomec.socomec.presentation.presenter;

import com.wit.socomec.socomec.data.model.Choice;
import com.wit.socomec.socomec.data.repository.Repository;
import com.wit.socomec.socomec.presentation.listener.MainNavigatorListener;
import com.wit.socomec.socomec.presentation.view.MessagingView;
import com.wit.socomec.socomec.presentation.viewmodel.MessageViewModel;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MessagingPresenter {
    private final Repository repository;
    private final MessagingView messagingView;
    private final MainNavigatorListener mainNavigatorListener;

    private Choice currentChoice;

    public MessagingPresenter(Repository repository, MessagingView messagingView, MainNavigatorListener mainNavigatorListener) {
        this.repository = repository;
        this.messagingView = messagingView;
        this.mainNavigatorListener = mainNavigatorListener;
    }

    public void onStart() {
        retrieveChoiceForId(0);
    }

    public void loadNextStep1(){
        messagingView.postMessage(new MessageViewModel(false, currentChoice.getAnswer1()));
        retrieveChoiceForId(currentChoice.getNextStep1());
    }

    public void loadNextStep2(){
        messagingView.postMessage(new MessageViewModel(false, currentChoice.getAnswer2()));
        retrieveChoiceForId(currentChoice.getNextStep2());
    }

    public void loadPreviousStep(){
        messagingView.postMessage(new MessageViewModel(false, "Je souhaite revenir à la question précédente"));
        retrieveChoiceForId(currentChoice.getPreviousStep());
    }

    private void retrieveChoiceForId(int id) {
        repository.retriveChoiceForId(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Choice>() {
                    @Override
                    public void onNext(Choice choice) {
                        currentChoice = choice;
                        messagingView.updateViewWithChoice(choice);
                        messagingView.postMessage(new MessageViewModel(true, choice.getQuestion()));
                    }
                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onComplete() {
                    }
                });
    }
}
