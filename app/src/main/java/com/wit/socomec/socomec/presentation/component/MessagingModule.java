package com.wit.socomec.socomec.presentation.component;

import android.view.View;

/**
 * Created by amiltonedev_lt030 on 05/04/2017.
 */

public interface MessagingModule {
    void displayErrorMessage(View view, String message);
    void displaySuccessMessage(View view, String message);
}
